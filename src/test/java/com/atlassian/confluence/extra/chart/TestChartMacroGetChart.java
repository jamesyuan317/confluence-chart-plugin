package com.atlassian.confluence.extra.chart;

import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.languages.Language;
import com.atlassian.confluence.languages.LanguageManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.links.LinkResolver;
import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

public class TestChartMacroGetChart extends TestCase
{
    private static final String[] CHART_TYPES = {
            "pie", "bar", "line", "area", "xyline", "xyarea", "xybar",
            "xystep", "xysteparea", "scatter", "timeseries",
    };

    @Mock
    private SettingsManager settingsManager;

    @Mock
    private LanguageManager languageManager;

    @Mock
    private AttachmentManager attachmentManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ThumbnailManager thumbnailManager;

    @Mock
    private WritableDownloadResourceManager exportDownloadResourceManager;

    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private LinkResolver linkResolver;
    
    @Mock
    private LocaleManager localeManager;
    
    @Mock
    private I18NBeanFactory i18nBeanFactory;

    private ChartMacro chartMacro;

    private Map<String, String> htmlChartContent;

    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        chartMacro = new ChartMacro(settingsManager, languageManager, attachmentManager, permissionManager, thumbnailManager, exportDownloadResourceManager, xhtmlContent, linkResolver, localeManager, i18nBeanFactory);
        htmlChartContent = createChartDataMap();

        Settings globalSettings = new Settings();
        globalSettings.setGlobalDefaultLocale("en_US");
        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        when(languageManager.getLanguages()).thenReturn(Collections.<Language>emptyList());
    }

    @Override
    protected void tearDown() throws Exception
    {
        settingsManager = null;
        languageManager = null;
        attachmentManager = null;
        permissionManager = null;
        thumbnailManager = null;
        exportDownloadResourceManager = null;
        xhtmlContent = null;
        linkResolver = null;
        super.tearDown();
    }

    private Map<String, String> createChartDataMap() throws IOException
    {
        StringBuilder resourceNameBuilder = new StringBuilder();
        Map<String, String> htmlChartContent = new HashMap<String, String>();

        for (String chartType : CHART_TYPES)
        {
            resourceNameBuilder.setLength(0);
            resourceNameBuilder.append("chartdata.").append(chartType).append(".html");

            Reader reader = null;
            Writer writer = null;

            try
            {
                reader = new BufferedReader(new InputStreamReader(
                        getClass().getClassLoader().getResourceAsStream(resourceNameBuilder.toString())));
                writer = new StringWriter();

                IOUtils.copy(reader, writer);

                htmlChartContent.put(chartType, writer.toString());

            }
            finally
            {
                IOUtils.closeQuietly(writer);
                IOUtils.closeQuietly(reader);
            }
        }

        return htmlChartContent;
    }

    public void testGetChartWithVariousChartTypes() throws ParseException
    {
        for (final String chartType : CHART_TYPES)
        {
            try
            {
                chartMacro.getChart(
                        new HashMap<String, String>()
                        {
                            {
                                put("type", chartType);
                                put("dateformat", "yyyy");
                            }
                        },
                        htmlChartContent.get(chartType)
                );
            }
            catch (MacroExecutionException unsupportedChartTypeFound)
            {
                fail(String.format("Unsupported chart type %s", chartType));
            }
        }
    }
}
