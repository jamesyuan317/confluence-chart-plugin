package com.atlassian.confluence.extra.chart;

import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.languages.LanguageManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.links.LinkResolver;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;

public class TestChartMacroPropertyAccessor extends TestCase
{
    @Mock
    private SettingsManager settingsManager;

    @Mock
    private LanguageManager languageManager;

    @Mock
    private AttachmentManager attachmentManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ThumbnailManager thumbnailManager;

    @Mock
    private WritableDownloadResourceManager exportDownloadResourceManager;

    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private LinkResolver linkResolver;
    
    @Mock
    private LocaleManager localeManager;
    
    @Mock
    private I18NBeanFactory i18nBeanFactory;

    private ChartMacro chartMacro;

    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        chartMacro = new ChartMacro(settingsManager, languageManager, attachmentManager, permissionManager, thumbnailManager, exportDownloadResourceManager, xhtmlContent, linkResolver, localeManager, i18nBeanFactory);
    }

    @Override
    protected void tearDown() throws Exception
    {
        settingsManager = null;
        languageManager = null;
        attachmentManager = null;
        permissionManager = null;
        thumbnailManager = null;
        exportDownloadResourceManager = null;
        xhtmlContent = null;
        linkResolver = null;
        super.tearDown();
    }

    public void testGetNonExistentIntegerParameterWithDefault() throws MacroExecutionException
    {
        assertEquals(new Integer(1), chartMacro.getIntegerParameter(
                new HashMap<String, String>(),
                "anInteger",
                1
        ));
    }

    public void testGetNonExistentIntegerParameterWithoutDefault() throws MacroExecutionException
    {
        assertNull(chartMacro.getIntegerParameter(
                new HashMap<String, String>(),
                "anInteger",
                null
        ));
    }

    public void testGetExistentIntegerParameterWithDefault() throws MacroExecutionException
    {
        assertEquals(new Integer(-1), chartMacro.getIntegerParameter(
                new HashMap<String, String>()
                {
                    {
                        put("anInteger", String.valueOf(-1));
                    }
                },
                "anInteger",
                0
        ));
    }

    public void testGetExistentIntegerParameterWithoutDefault() throws MacroExecutionException
    {
        assertEquals(new Integer(-1), chartMacro.getIntegerParameter(
                new HashMap<String, String>()
                {
                    {
                        put("anInteger", String.valueOf(-1));
                    }
                },
                "anInteger",
                null
        ));
    }

    public void testGetExistentDoubleParameterAsIntegerWithDefault()
    {
        try
        {
            chartMacro.getIntegerParameter(
                    new HashMap<String, String>()
                    {
                        {
                            put("anInteger", String.valueOf(1.0));
                        }
                    },
                    "anInteger",
                    0
            );
            fail("Expected exception not raised for invalid integer");
        }
        catch (MacroExecutionException invalidInteger)
        {
            // Success
        }
    }

    public void testGetNonExistentIntegerParameterWithThreshold() throws MacroExecutionException
    {
        assertEquals(1, chartMacro.getIntegerParameter(
                new HashMap<String, String>(),
                "anInteger",
                1,
                Integer.MAX_VALUE
        ));
    }

    public void testGetExistentIntegerParameterWithLargerThreshold() throws MacroExecutionException
    {
        assertEquals(1, chartMacro.getIntegerParameter(
                new HashMap<String, String>()
                {
                    {
                        put("anInteger", String.valueOf(10));
                    }
                },
                "anInteger",
                1,
                11
        ));
    }

    public void testGetExistentIntegerParameterWithSmallerThreshold() throws MacroExecutionException
    {
        assertEquals(10, chartMacro.getIntegerParameter(
                new HashMap<String, String>()
                {
                    {
                        put("anInteger", String.valueOf(10));
                    }
                },
                "anInteger",
                1,
                9
        ));
    }

    public void testGetNonExistentDoubleParameterWithDefault() throws MacroExecutionException
    {
        assertEquals(1d, chartMacro.getDoubleParameter(
                new HashMap<String, String>(),
                "aDouble",
                1d
        ));
    }

    public void testGetNonExistentDoubleParameterWithoutDefault() throws MacroExecutionException
    {
        assertNull(chartMacro.getDoubleParameter(
                new HashMap<String, String>(),
                "aDouble",
                null
        ));
    }

    public void testGetExistentDoubleParameterWithDefault() throws MacroExecutionException
    {
        assertEquals(-1d, chartMacro.getDoubleParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aDouble", String.valueOf(-1d));
                    }
                },
                "aDouble",
                0d
        ));
    }

    public void testGetExistentDoubleParameterWithoutDefault() throws MacroExecutionException
    {
        assertEquals(-1d, chartMacro.getDoubleParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aDouble", String.valueOf(-1d));
                    }
                },
                "aDouble",
                null
        ));
    }

    public void testGetExistentStringParameterAsDoubleWithDefault()
    {
        try
        {
            chartMacro.getDoubleParameter(
                    new HashMap<String, String>()
                    {
                        {
                            put("aDouble", "invalid");
                        }
                    },
                    "aDouble",
                    0d
            );
            fail("Expected exception not raised for invalid double");
        }
        catch (MacroExecutionException invalidInteger)
        {
            // Success
        }
    }

    public void testGetNonExistentBooleanParameter() throws MacroExecutionException
    {
        assertTrue(chartMacro.getBooleanParameter(
                new HashMap<String, String>(),
                "aBoolean",
                true
        ));
    }

    public void testGetExistentBooleanParameter() throws MacroExecutionException
    {
        assertFalse(chartMacro.getBooleanParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aBoolean", "false");
                    }
                },
                "aBoolean",
                true
        ));
    }

    public void testGetExistentStringParameterAsBoolean() throws MacroExecutionException
    {
        assertTrue(chartMacro.getBooleanParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aBoolean", "shouldResultInTrue");
                    }
                },
                "aBoolean",
                true
        ));
    }


    public void testGetNonExistentStringParameterWithDefault() throws MacroExecutionException
    {
        assertEquals("defaultString", chartMacro.getStringParameter(
                new HashMap<String, String>(),
                "aString",
                "defaultString"
        ));
    }

    public void testGetNonExistentStringParameterWithoutDefault() throws MacroExecutionException
    {
        assertNull(chartMacro.getStringParameter(
                new HashMap<String, String>(),
                "aString",
                null
        ));
    }

    public void testGetExistentStringParameterWithDefault() throws MacroExecutionException
    {
        assertEquals("userSpecifiedString", chartMacro.getStringParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aString", "userSpecifiedString");
                    }
                },
                "aString",
                "defaultString"
        ));
    }
}
