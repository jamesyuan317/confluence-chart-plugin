package it.com.atlassian.confluence.extra.chart;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.ArrayUtils;
import org.xml.sax.SAXException;

public class ChartMacroTestCase extends AbstractConfluencePluginWebTestCase
{
    private long idOfPageContainingChartMacro;

    protected void setUp() throws Exception
    {
        final PageHelper pageHelper;

        super.setUp();

        pageHelper = getPageHelper();
        pageHelper.setSpaceKey("TST");
        pageHelper.setTitle("Chart Macro Test");
        pageHelper.setContent(StringUtils.EMPTY);

        assertTrue(pageHelper.create());

        idOfPageContainingChartMacro = pageHelper.getId();
    }

    // test this
    public void testCreateSimpleChart() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper(idOfPageContainingChartMacro);

        assertTrue(pageHelper.read());

        pageHelper.setContent(
                "{chart}\n" +
                "|| || Democrat || Republican || Independent ||\n" +
                "|| Mascots | 40 | 40 | 20 |\n" +
                "{chart}");

        assertTrue(pageHelper.update());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        String chartImageXpath = "//div[@class='wiki-content']//img";

        assertElementPresentByXPath(chartImageXpath);

        String imageSource = getElementAttributByXPath(chartImageXpath, "src");
        assertTrue(imageSource.matches("^.*/download/temp/chart.*\\.png$"));
    }

    public void testCreateSimpleChartAndOutputToAttachment() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper(idOfPageContainingChartMacro);
        final AttachmentHelper attachmentHelper;

        assertTrue(pageHelper.read());

        pageHelper.setContent(
                "{chart:attachment=file.png}\n" +
                "|| || Democrat || Republican || Independent ||\n" +
                "|| Mascots | 40 | 40 | 20 |\n" +
                "{chart}");

        assertTrue(pageHelper.update());
        gotoPage(pageHelper.getId()); // Trigger the macro's execution so the chart gets saved to an attachment
        

        /* Check if an attachment is created */
        assertTrue(ArrayUtils.contains(pageHelper.getAttachmentFileNames(), "file.png"));

        attachmentHelper = getAttachmentHelper(pageHelper.getId(), "file.png");
        attachmentHelper.read();

        assertEquals("image/png", attachmentHelper.getContentType());
        assertTrue(0 < attachmentHelper.getContentLength());
        assertEquals(getConfluenceWebTester().getCurrentUserName(), attachmentHelper.getCreator());
    }

    private void gotoPage(long pageId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + pageId);
    }

    public void testCreateSimpleChartAndOutputToAttachmentOfAnotherPage() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper(idOfPageContainingChartMacro);
        final PageHelper anotherPageHelper = getPageHelper();
        final AttachmentHelper attachmentHelper;

        anotherPageHelper.setSpaceKey("TST");
        anotherPageHelper.setTitle("The page which the chart should be attached to");
        anotherPageHelper.setContent("Foobar");
        assertTrue(anotherPageHelper.create());



        assertTrue(pageHelper.read());

        pageHelper.setContent(
                "{chart:attachment=The page which the chart should be attached to^file.png}\n" +
                "|| || Democrat || Republican || Independent ||\n" +
                "|| Mascots | 40 | 40 | 20 |\n" +
                "{chart}");

        assertTrue(pageHelper.update());
        gotoPage(pageHelper.getId()); // Trigger the macro's execution so the chart gets saved to an attachment

        /* Check if an attachment is created */
        assertTrue(ArrayUtils.contains(anotherPageHelper.getAttachmentFileNames(), "file.png"));

        /* Check that the page which contains the macro does not have an attachment. */
        assertEquals(0, pageHelper.getAttachmentFileNames().length);

        attachmentHelper = getAttachmentHelper(anotherPageHelper.getId(), "file.png");
        attachmentHelper.read();

        assertEquals("image/png", attachmentHelper.getContentType());
        assertTrue(0 < attachmentHelper.getContentLength());
        assertEquals(getConfluenceWebTester().getCurrentUserName(), attachmentHelper.getCreator());

        assertTrue(anotherPageHelper.delete());
    }

    public void testCreateSimpleChartAndOutputToAttachmentOfAnotherPageInAnotherSpace() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper(idOfPageContainingChartMacro);
        final PageHelper anotherPageHelper = getPageHelper();
        final AttachmentHelper attachmentHelper;

        anotherPageHelper.setSpaceKey("ds");
        anotherPageHelper.setTitle("Welcome to the Confluence Demonstration Space");

        anotherPageHelper.setId(anotherPageHelper.findBySpaceKeyAndTitle());
        assertTrue(anotherPageHelper.read());



        assertTrue(pageHelper.read());

        pageHelper.setContent(
                "{chart:attachment=ds:Welcome to the Confluence Demonstration Space^file.png}\n" +
                "|| || Democrat || Republican || Independent ||\n" +
                "|| Mascots | 40 | 40 | 20 |\n" +
                "{chart}");

        assertTrue(pageHelper.update());
        gotoPage(pageHelper.getId()); // Trigger the macro's execution so the chart gets saved to an attachment

        /* Check if an attachment is created */
        assertTrue(ArrayUtils.contains(anotherPageHelper.getAttachmentFileNames(), "file.png"));

        /* Check that the page which contains the macro does not have an attachment. */
        assertEquals(0, pageHelper.getAttachmentFileNames().length);

        attachmentHelper = getAttachmentHelper(anotherPageHelper.getId(), "file.png");
        attachmentHelper.read();

        assertEquals("image/png", attachmentHelper.getContentType());
        assertTrue(0 < attachmentHelper.getContentLength());
        assertEquals(getConfluenceWebTester().getCurrentUserName(), attachmentHelper.getCreator());

        assertTrue(attachmentHelper.delete());
    }

    public void testAttachmentIsNotCreatedIfImageIsSame() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper(idOfPageContainingChartMacro);
        final AttachmentHelper attachmentHelper;

        assertTrue(pageHelper.read());

        pageHelper.setContent(
                "{chart:attachment=file.png}\n" +
                        "|| || Democrat || Republican || Independent ||\n" +
                        "|| Mascots | 40 | 40 | 20 |\n" +
                        "{chart}");

        assertTrue(pageHelper.update());
        gotoPage(pageHelper.getId()); // Trigger the macro's execution so the chart gets saved to an attachment


        /* Check if an attachment is created */
        assertTrue(ArrayUtils.contains(pageHelper.getAttachmentFileNames(), "file.png"));

        attachmentHelper = getAttachmentHelper(pageHelper.getId(), "file.png");
        attachmentHelper.read();

        assertEquals("image/png", attachmentHelper.getContentType());

        // reload the page and make sure no new version of attachment is created
        gotoPage(pageHelper.getId());
        // go to attachment page
        gotoPage("/pages/viewpageattachments.action?pageId=" + pageHelper.getId());
        assertElementNotPresentByXPath("//div[@class='attachment-section']//table[@class='aui tableview attachments']//tr[@data-attachment-version='2']");
        assertElementPresentByXPath("//div[@class='attachment-section']//table[@class='aui tableview attachments']//tr[@data-attachment-version='1']");
    }

}
